from django.db import models
from django.utils import timezone 
from datetime import datetime, date

class Status(models.Model):    
    date_time     = models.DateTimeField(default=timezone.now)   
    status_message  = models.CharField(max_length=300)

class Subscriber(models.Model):
    name = models.CharField(max_length=35)
    email = models.EmailField(unique=True)
    password = models.CharField(max_length=35)
