from django import forms
from .models import *
from django.forms import ModelForm

class Status_Form(forms.ModelForm):

    class Meta:
        model = Status
        fields = ('date_time', 'status_message',)
        widgets = {'date_time': forms.DateTimeInput(attrs={'class': 'form-control',}) ,
		'status_message': forms.TextInput(attrs={'class': 'form-control',}),
		}

class Subsc_Form(forms.ModelForm):

    class Meta:
        model = Subscriber
        fields = ('name', 'email', 'password')
        widgets = {
            
            'name': forms.TextInput(
                attrs={'class': 'form-control', 'id':'nameSubscriber', 'placeholder': "What's your name?", 'maxlength': 35}),
		
            'email': forms.TextInput(
                attrs={'type':'email', 'class': 'form-control','id':'emailSubscriber','placeholder': 'your email...', 'maxlength': 55}),
        
            'password': forms.TextInput(
                attrs={'type':'password', 'class': 'form-control', 'id':'passwSubscriber', 'placeholder': 'your password...', 'maxlength': 15}),
		}

