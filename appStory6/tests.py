from django.test import TestCase, Client
from django.urls import resolve
from .views import status, profil, favBook, subsc   
from .models import Status
from .forms import Status_Form
    
    # Create your tests here.
class appStory6UnitTest(TestCase):
    
    def test_appStory9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_appStory9_using_status_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, status)

    def test_profil_url_is_exist(self):
        response = Client().get('/story9/profil/')
        self.assertEqual(response.status_code, 200)

    def test_ros_story9_using_profil_func(self):
        found = resolve('/story9/profil/')
        self.assertEqual(found.func, profil)

    def test_book_url_is_exist(self):
        response = Client().get('/story9/book/')
        self.assertEqual(response.status_code, 200)
    
    def test_ros_story9_using_favBook_func(self):
        found = resolve('/story9/book/')
        self.assertEqual(found.func, favBook)

    def test_story10_url_is_exist(self):
        response = Client().get('/story10/')
        self.assertEqual(response.status_code, 200)

    def test_ros_story10_using_subsc_func(self):
        found = resolve('/story10/')
        self.assertEqual(found.func, subsc)

    def test_landing_page_content_is_written(self):
        response = Client().get('/story9/')
        landing_page_content = response.content.decode('utf8')
        #content cannot be null
        self.assertIsNotNone(landing_page_content)

        #content is filled with 30 character
        self.assertTrue(len(landing_page_content) >= 30)

    def test_landing_page_have_a_String(self):
        response = Client().get('/story9/')
        landing_page_content = response.content.decode('utf8')
        self.assertIn('Hello, Apa kabar?', landing_page_content)

    def test_model_can_create_new_status(self):
        # Creating a new status
        new_status = Status.objects.create(status_message = 'mengerjakan story 9 ppw')
        # Retrieving all status
        counting_all_status_message = Status.objects.all().count()
        self.assertEqual(counting_all_status_message, 1)

    def test_ros_story9_using_new_status_template(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'new_status.html')

    def test_ros_story9_using_profil_template(self):
        response = Client().get('/story9/profil/')
        self.assertTemplateUsed(response, 'myProfil.html')
    
    def test_ros_story9_using_list_book_template(self):
        response = Client().get('/story9/book/')
        self.assertTemplateUsed(response, 'list_book.html')
    
    def test_status_validation_for_blank_items(self):
        form = Status_Form(data={'status_message': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['status_message'],
            ["This field is required."]
        )

    def test_new_status_post_success_and_render_the_result(self):
        test = 'Ros Status'
        response_post = Client().post('/story9/', { 'status_message': test})
        self.assertEqual(response_post.status_code, 200)
    
        response= Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_story9_post_error_and_render_the_result(self):
        test = 'xxx'
        response_post = Client().post('/story9/', {'status_message': ''})
        self.assertEqual(response_post.status_code, 200)
    
        response= Client().get('/story9/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

  
    
    



