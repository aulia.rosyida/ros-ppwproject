from django.urls import path
from appStory6 import views

urlpatterns = [
    path('', views.status, name='status'),
    path('story9/', views.status, name='status'),
    path('story9/profil/', views.profil, name='profil'),
    path('story9/book/', views.favBook, name='favBook'),
    path('story10/', views.subsc, name='subsc'),
    path('story10/validate/', views.user_validation, name='validCheck'),
    path('story9/dtJSON/', views.dtJSON, name='data'),
    # path('story9/diary/', views.mydiary, name='diary'),

]