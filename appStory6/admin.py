from django.contrib import admin
from .models import Status, Subscriber

# Register your models here.

#status Form
admin.site.register(Status)

#subscriber Form
class RegSubcrAdmin(admin.ModelAdmin):
     list_display= ['name', 'email', 'password']

admin.site.register(Subscriber, RegSubcrAdmin)
