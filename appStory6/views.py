from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.utils import timezone 
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator
import requests
import json
import re

import urllib.request, json, requests

from .models import Status, Subscriber
from .forms import Status_Form, Subsc_Form

# Create my views here.
response = {}
def status(request): 
    message = Status.objects.all().values() 
    if request.method == "POST":
        form = Status_Form(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('status'))
    else: 
        form = Status_Form() 
    response = {'form': form, 'kegiatan':message }    
    return render(request, 'new_status.html',response)

def profil(request):    
    return render(request, 'myProfil.html', {})

def first(request): 
    return render(request, 'new_status.html', {})

def favBook(request): 
    return render(request, 'list_book.html', {})

def dtJSON(request):
    search = request.GET.get('search')
    data = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + search)
    return HttpResponse(data, content_type='application/json')

# function POST request subscriber
@csrf_exempt
def subsc(request):

    # pagination page
    get_user_acccount = Subscriber.objects.all()
    paginator = Paginator(get_user_acccount, 8)
    page = request.GET.get('page')
    get_user_paginate = paginator.get_page(page)

    # post method save user
    if request.method == "POST":
        form_value  = Subsc_Form(request.POST or None)
        name        = request.POST['name']
        email       = request.POST['email']
        password    = request.POST['password']
        create_user = Subscriber(email=email, name=name, password=password)
        create_user.save()

    else:
        form_value = Subsc_Form()

    return render(request, 'regis_subscriber.html', {'Subsc_Form': form_value, 'user_account': get_user_paginate})


# function for validate email user , name , password 
@csrf_exempt
def user_validation(request):

    # boolean flag for chack each form
    nama_validate_check = False
    password_validate_check = False
    email_validate_check = False

    if request.method == "POST":
        name = request.POST['name']
        email = request.POST['email']
        password = request.POST['password']

        # using regex check validate email address
        if 5 <= len(name) <= 30:
            nama_validate_check = True

        if len(email) >= 8:
            email_validate_check = bool(re.match("^.+@(\[?)[a-zA-Z0-9-.]+.([a-zA-Z]{2,3}|[0-9]{1,3})(]?)$", email))

        if 8 <= len(password) <= 14:
            password_validate_check = True

        if not nama_validate_check:
            return HttpResponse(json.dumps({'message': 'Name must be 5 to 30 characters or fewer.'}),
                                content_type="application/json")

        if not email_validate_check:
            return HttpResponse(json.dumps({'message': 'email must contain "@" character'}),
                                content_type="application/json")

        if not password_validate_check:
            return HttpResponse(json.dumps({'message': 'Your password must contain at least 8 characters'}),
                                content_type="application/json")

        user_filter_email = Subscriber.objects.filter(email=email)

        if len(user_filter_email) > 0:
            return HttpResponse(json.dumps({'message': 'Email already registered'}), content_type="application/json")

        return HttpResponse(json.dumps({'message': 'All fields are valid'}), content_type="application/json")

    else:
        return HttpResponse(json.dumps({'message': "There's something wrong. Try again."}),
                            content_type="application/json")

# def mydiary(request):
#     data = requests.POST('/story9')
