import time
import unittest
from django.test import TestCase, Client

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

class FunctionalTest(unittest.TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        chrome_options.add_argument("--proxy-server='direct://'")
        chrome_options.add_argument("--proxy-bypass-list=*")
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25) 
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.implicitly_wait(3)
        self.selenium.quit()  
        super(FunctionalTest, self).tearDown()       

    def test_can_make_a_new_status(self):
        selenium=self.selenium
        selenium.get('https://ros-story6.herokuapp.com/') 
        time.sleep(5)
        # find the form element
        newStatus = selenium.find_element_by_id('id_status_message')

        # Fill the form with message "Coba Coba"
        newStatus.send_keys('Coba Coba')

        # submitting the form
        newStatus.submit()

        #check whether or not string "Coba Coba" in page_source 
        self.assertIn('Coba Coba', self.selenium.page_source)
        time.sleep(5)

    def test_layout_is_updateTime_column_at_the_right_coordinate(self):
        self.selenium.get('https://ros-story6.herokuapp.com/') 
        e = self.selenium.find_element_by_id('updateTime')
        location = e.location
        coor_X = location['x']
        coor_Y = location['y']
        self.assertEqual(coor_X, 0)
        self.assertEqual(coor_Y, 255)

    def test_layout_is_statusQ_column_at_the_right_coordinate(self):
        self.selenium.get('https://ros-story6.herokuapp.com/') 
        f = self.selenium.find_element_by_id('statusQ')
        locationf = f.location
        coorf_X = locationf['x']
        coorf_Y = locationf['y']
        self.assertEqual(coorf_X, 296)
        self.assertEqual(coorf_Y, 255)

    def test_css_is_fontSize_is_right(self):
        self.selenium.get('https://ros-story6.herokuapp.com/') 
        size = self.selenium.find_element_by_id("updateTime").value_of_css_property("font-size")
        self.assertEqual(size, "16px")

    def test_css_is_jenis_fontnya_is_right(self):
        self.selenium.get('https://ros-story6.herokuapp.com/') 
        kindOfFont = self.selenium.find_element_by_id('updateTime').value_of_css_property("font")
        self.assertEqual(kindOfFont, 'normal normal 700 normal 16px / 22.8571px "Helvetica Neue", Helvetica, Arial, sans-serif')

if __name__ == '__main__': # 
    unittest.main(warnings='ignore') #

